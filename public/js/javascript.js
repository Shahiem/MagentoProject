$(document).ready(function() {
    $('#listTable').DataTable( {
        "language": {
            "lengthMenu": "_MENU_ per pagina",
            "zeroRecords": "Helaas niets gevonden :(",
            "info": "Pagina _PAGE_ van _PAGES_",
            "infoEmpty": "Er zijn geen facturen gevonden",
            "infoFiltered": "(Gefilterd van _MAX_ facturen)"
        }
    });
} );