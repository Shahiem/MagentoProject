<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helpers\InvoiceHelper;

class InvoiceController extends Controller 
{

    public function __construct() 
    {  
        $this->invoice = new InvoiceHelper();
    }

    public function index() 
    {
        $items = $this->invoice->listView();

        return view('admin/invoices/list', [
            'items' => $items
        ]);
    } 

    public function create() 
    {
        return view('admin/invoices/create', [
        ]);
    } 

    public function update($id) 
    {
        // Return items etc
        $invoice = $this->invoice->updateView($id); // Parameter: increment_id

        // Return order information
        $order = $this->invoice->orderView($invoice['order_increment_id']); // Parameter: order_increment_id
        
        return view('admin/invoices/update', [
            'order' => $order,
            'invoice_number' => $id,
            'items' => $invoice['items']
        ]);
    } 

}