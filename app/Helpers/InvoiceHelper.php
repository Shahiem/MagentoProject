<?php

namespace App\Helpers;

use SoapClient;
use App\Helpers\MagentoHelper;

class InvoiceHelper
{

    public function __construct() 
    {  
        $this->magento = new MagentoHelper();
    }

    public function listView() 
    {  
		return $this->magento->call('sales_order_invoice.list');
	}

    public function updateView($id) 
    {  
		return $this->magento->call('sales_order_invoice.info', $id);
	}

    public function orderView($id) 
    {  
		return $this->magento->call('sales_order.info', $id);
	}

}