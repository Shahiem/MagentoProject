<?php

namespace App\Helpers;

use SoapClient;

class MagentoHelper
{

    public function __construct() 
    {  
		$this->clientRequest = new SoapClient('http://comm.dev.local/api/soap?wsdl=1');

		// Login as user (1: ApiUser | 2: ApiKey)
		$this->clientSession = $this->clientRequest->login('Shahiem', 'test123');
	}

    public function call($view, $attr = NULL) 
    {  
    	// Call a magento view
		return $this->clientRequest->call($this->clientSession, $view, $attr);
	}

}