<!DOCTYPE html>
<html>
<head>
	<title>Facturatie systeem - Shahiem</title>

	<link rel="stylesheet" type="text/css" href="{{ asset('css/theme.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/semantic.css') }}">
</head>
<body>
	<div class="ui grid">
		<div class="three wide column">
			<div class="ui secondary vertical pointing menu">
				<a href="{{ url('/') }}" class="active item">
				    Facturen <span class="ui blue label">1</span> &nbsp;
				    <i class="icon bar chart"></i>
				</a>

				<a class="item disabled"><i class="icon dollar chart"></i> Betalingen</a>
				<a class="item disabled"><i class="icon bar chart"></i> BTW</a>
				<a class="item disabled"><i class="icon bar chart"></i> Statistieken</a>
			</div>
		</div>

		<div class="ten wide column">
			@yield('content')
		</div>
	</div>

    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/semantic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
</body>
</html>