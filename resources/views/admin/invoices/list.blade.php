@extends('theme.layout')

@section('content')
<div id="breadCrumb" class="ui segment">
	<div class="ui breadcrumb">
	  	<a class="section">Admin</a>
	 	<span class="divider">/</span>

	  	<a class="section">Facturen</a>
	  	<span class="divider">/</span>

	  	<div class="active section">Overzicht</div>
	</div>
</div>

<button class="ui blue button">
  	<i class="icon plus"></i>
  	Nieuw
</button>

<i class="icon trash"></i><br><br>

<table id="listTable" class="ui very basic collapsing celled table" style="width: 100%;">
    <thead>
      <tr>
          <th>Factuurnummer</th>
          <th>Aangemaakt op</th>
          <th>Totaal</th>
          <th></th>
      </tr>
    </thead>
  	<tbody>
  		@foreach($items as $item)
    	<tr>
      		<td>#{{ $item['increment_id'] }}</td>
      		<td>{{ date('d-m-Y H:i', strtotime($item['created_at'])) }}</td>
      		<td>&euro;{{ number_format($item['grand_total'], 2, ',', ' ') }}</td>
      		<td>
              <a href="{{ url('invoice/update/'.$item['increment_id']) }}"><i class="icon pencil"></i></a>
              <i class="icon trash"></i>
      		</td>
    	</tr>
    	@endforeach
    </tbody>
</table>
@stop