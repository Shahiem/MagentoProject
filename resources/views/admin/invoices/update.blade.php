@extends('theme.layout')

@section('content')
<div id="breadCrumb" class="ui segment">
	<div class="ui breadcrumb">
	  	<a class="section">Admin</a>
	 	<span class="divider">/</span>

	  	<a class="section">Facturen</a>
	  	<span class="divider">/</span>

	  	<div class="active section">Wijzig factuur: #{{ $invoice_number }}</div>
	</div>
</div>

<div class="ui form">
  	<div class="two fields">
	    <div class="field">
	      <label>Factuurnummer</label>
	      <input type="text" value="{{ $invoice_number }}">
	    </div>
  	</div>

  	<h3>Verzending informatie</h3>
	<div class="field">
	     <label>Adres</label>
	    <input type="text" value="{{ $order['shipping_address']['street'] }}">
	</div>

  	<div class="two fields">
	    <div class="field">
	      	<label>Postcode</label>
	      	<input type="text" value="{{ $order['shipping_address']['postcode']  }}">
	    </div>

	    <div class="field">
	      	<label>Plaats</label>
	      	<input type="text" value="{{ $order['shipping_address']['city'] }}">
	    </div>
  	</div>

	<iframe
	  width="100%"
	  height="200"
	  frameborder="0" style="border:0"
	  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBcac5y7P23_n39_c1j6uOSs--FicEoKNQ
	    &q={{ $order['shipping_address']['street'] }}+{{ $order['shipping_address']['postcode']  }}+{{ $order['shipping_address']['city'] }}" allowfullscreen>
	</iframe>

  	<h3>Betalings informatie</h3>
	<div class="field">
	     <label>Adres</label>
	    <input type="text" value="{{ $order['billing_address']['street'] }}">
	</div>

  	<div class="two fields">
	    <div class="field">
	      	<label>Postcode</label>
	      	<input type="text" value="{{ $order['billing_address']['postcode']  }}">
	    </div>

	    <div class="field">
	      	<label>Plaats</label>
	      	<input type="text" value="{{ $order['billing_address']['city'] }}">
	    </div>
  	</div>

	<iframe
	  width="100%"
	  height="200"
	  frameborder="0" style="border:0"
	  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBcac5y7P23_n39_c1j6uOSs--FicEoKNQ
	    &q={{ $order['billing_address']['street'] }}+{{ $order['billing_address']['postcode']  }}+{{ $order['billing_address']['city'] }}" allowfullscreen>
	</iframe>
</div>

<div class="ui segment">
	<h4>Bestellingen</h4>
	<table class="ui very basic collapsing celled table" style="width: 100%;">
	    <thead>
	      	<tr>
	          	<th class="five wide">Product</th>
	          	<th>Prijs</th>
	          	<th>Aantal</th>
	          	<th>Totaal prijs</th>
	        </tr>
	    </thead>
	  	<tbody>
	  	  	@foreach($items as $item)
	        <tr>
	          	<td>{{ $item['name'] }}</td>
	          	<td>&euro;{{ number_format($item['price'], 2, ',', ' ') }}</td>
				<td>{{ round($item['qty']) }}x</td>
	          	<td>&euro;{{ number_format($item['row_total'], 2, ',', ' ') }}</td>
	        </tr>
	        @endforeach	
	    </tbody>
	</table>
</div>

<button class="ui large blue button">Opslaan</button>
<button class="ui large button">Opslaan en nieuw</button>
<i class="trash icon"></i>

@stop